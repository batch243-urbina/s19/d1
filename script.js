// console.log(`TGIF!`);

// Conditional statements
// control the flow of the program

// if, else if and else statement

let numA = -1;
// if statement - will execute if a specified condition is true
if (numA < 0) {
  console.log(`Hello!`);
}

// update the variable
numA = 0;
if (numA < 0) {
  console.log(`Hello!`);
}

let city = "New York";
if (city === "New York") {
  console.log(`Welcome to New York!`);
}

// else if - if previous conditions are false

let numH = 1;

if (numH < 0) {
  console.log(`Hello from NumH`);
} else if (numH > 0) {
  console.log(`Hi I'm numH!`);
}

if (numH > 0) {
  console.log(`Hello from NumH`);
} else if (numH === 0) {
  console.log(`Hi I'm numH!`);
} else if (numH < 0) {
  console.log(`Hi I'm numH!`);
}

city = "Tokyo";
if (city === "New York") {
  console.log(`Welcome to New York!`);
} else if (city === "Tokyo") {
  console.log(`Welcome to Tokyo!`);
}

// else - any other conditions not met

numH = 2;

if (numH < 0) {
  console.log(`Hello I'm numH`);
} else if (numH > 2) {
  console.log(`numH greater than 2`);
} else if (numH > 3) {
  console.log(`numH greater than 3`);
} else {
  console.log(`numH is free`);
}

// if, else if, and else with functions

let message;

function determineTyphoonIntensity(windSpeed) {
  if (windSpeed < 0) {
    return `Not a valid argument`;
  } else if (windSpeed > 0 && windSpeed < 30) {
    return `Not a typhoon yet`;
  } else if (windSpeed <= 60) {
    return `Tropical depression detected`;
  } else if (windSpeed >= 61 && windSpeed <= 88) {
    return `Tropical storm detected`;
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return `Severe tropical storm detected`;
  } else {
    return `Typhoon detected`;
  }
}

message = determineTyphoonIntensity(70);
console.log(message);

// console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code

if (message === `Tropical storm detected`) {
  console.warn(message);
}

// TRUTHY AND FALSY
// Truthy - considered true when encountered in Boolean context unless defined otherwise

// Falsy - exceptions for truthy
// 1. false
// 2. 0
// 3. -0
// 4. ""
// 5. null
// 6. undefined
// 7. NaN

// Truthy samples
if (true) {
  console.log(`Truthy`);
}

if (1) {
  console.log(`Truthy`);
}

if ([]) {
  console.log(`Truthy`);
}

// Falsy examples
if (false) {
  console.log(`Falsy`);
}
if (0) {
  console.log(`Falsy`);
}
if (undefined) {
  console.log(`Falsy`);
}

// Ternary Operators

let ternaryResult = 1 > 18 ? 1 : 2;
console.log(ternaryResult);

// Multiple statement

let firstName;

function isOfLegalAge() {
  firstName = "John";
  return `You are of the legal age`;
}
function isUnderAge() {
  firstName = "Jane";
  return `You are under age limit`;
}

// let age = parseInt(prompt(`What is your age?`));
// console.log(age);

// let legalAge = age >= 18 ? isOfLegalAge() : isUnderAge();
// console.log(
//   `Result of Ternary Operator in Functions: ${legalAge}, ${firstName}`
// );

// SWITCH STATEMENT - evaluates an expression and matches the expression's value to a case clause
/* switch(expression){
	case value:
		statement;
		break;
	default:
		statement;
		break
}
*/

// let day = prompt(`What day of the week is it today?`).toLowerCase();
// console.log(day);

// switch (day) {
//   case "monday":
//     console.log(`The color of the day is Red!`);
//     break;
//   case "tuesday":
//     console.log(`The color of the day is Orange!`);
//     break;
//   case "wednesday":
//     console.log(`The color of the day is Yellow!`);
//     break;
//   case "thursday":
//     console.log(`The color of the day is Green!`);
//     break;
//   case "friday":
//     console.log(`The color of the day is Blue!`);
//     break;
//   case "saturday":
//     console.log(`The color of the day is Indigo!`);
//     break;
//   case "sunday":
//     console.log(`The color of the day is Violet!`);
//     break;
//   default:
//     console.log(`Please input a valid!`);
// }

// Try-catch-finally statement
// Commonly used for error handling
// error that is not necessarily an error in the context of our code
// Efficient coding
// Used to specify a response whenever an error is received

function showIntensityAlert(windSpeed) {
  try {
    alerat(determineTyphoonIntensity(110));
  } catch (error) {
    console.warn(error.message);
  } finally {
    alert(`Intensity updates will show alert.`);
  }
}

showIntensityAlert(110);
